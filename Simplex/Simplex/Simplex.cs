﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplex
{
    class Simplex
    {
        // FERTIG? Ja oder Nein?
        private bool fertig;
        // Ausgangsprodukt = x1, x2, ...
        private int anzX; // Anzahl der Ausgangsprodukte
        // Endprodukte = s1, s2, ...
        private int anzS; // Anzahl der Endprodukte
        // Anzahl der Ausgangs+Endprodukte
        private int anzXS;
        // Anzahl der Zeilen, welche insgesamt eingetragen werden
        private int anzZeilen;
        // die ganzen Zahlen der Endprodukte und Ausgangsprodukte
        private double[,] zahlen;   // Zeile, Spalte
        // RechteSeite und Quotient
        private double[] rs, q;
        // Bezeichnungen --> LinkeSeite und ObereSeite
        private string[] bezLinks, bezOben;   // was ganz links steht
        // Pivot
        private int pivotSpalte, pivotZeile;
        private double pivotElement;
        private double[] pivotSpalteWerte;

        public Simplex(int anzEnd, int anzAus)
        {
            // Variablen zuweisen
            this.fertig = false;    // dass Simplex noch nicht Fertig ist
            this.anzX = anzEnd;
            this.anzS = anzAus;
            this.anzXS = anzAus + anzEnd;
            this.anzZeilen = anzS + 1;
            this.zahlen = new double[anzZeilen, anzXS];
            this.rs = new double[anzZeilen];
            this.q = new double[anzZeilen-1];
            this.pivotSpalteWerte = new double[anzZeilen];

            // füllen des Simplex
            for (int i = 0; i < anzZeilen; i++)
            {
                fillLine(i, new double[] { 0, 0, 0, 0 });
            }
            // Bezeichnung Links
            this.bezLinks = new string[anzZeilen];
            for (int i = 0; i < anzZeilen; i++)
            {
                if (i == 0)
                    bezLinks[i] = "Z";
                else
                    bezLinks[i] = "S" + i;
            }
            // Bezeichnung Oben
            this.bezOben = new string[anzXS + 2];
            for (int i = 1; i <= (anzXS + 2); i++)
            {
                if (i <= anzX)
                    bezOben[i-1] = "X" + i;
                else
                {
                    if (i <= anzXS)
                        bezOben[i-1] = "S" + (i-anzX);
                    if (i == (anzXS + 1))
                        bezOben[i-1] = "RS";
                    if (i == (anzXS + 2))
                        bezOben[i-1] = "Q";
                }
            }
        }

        public void fillLine(int z, double[] werte)
        {
            for (int i = 0; i < anzX; i++)  // Endprodukte --> X
            {
                zahlen[z, i] = werte[i];
            }

            for (int i = anzX; i < anzXS; i++)  // Ausgangsprodukte --> S (Rohstoffe)
            {
                if (z != 0)
                {
                    if (z == (i-anzX+1))
                        zahlen[z, i] = 1;
                    else
                        zahlen[z, i] = 0;
                }
                else
                    zahlen[z, i] = 0;
            }

            // Rechte Seite + Quotient
            rs[z] = werte[werte.Length - 1];    // Rechte Seite füllen
            if(z != 0)
                q[z-1] = 0;    // Quotienten füllen
        }

        public void quotientenBerechnen()
        {
            // PivotSpalte
            double max = Double.MinValue;
            for (int i = 0; i <= anzX; i++)
            {
                if (max < zahlen[0, i])
                {
                    max = zahlen[0, i];
                    pivotSpalte = i;
                }
            }
            Console.WriteLine("PivotSpalte berechnen: " + max + " in Spalte: " + pivotSpalte);
            // wenn PivotSpalte der Wert "0" ist, dann ist es Fertig
            if (max == 0)
                fertig = true;

            // Quotienten ausrechnen
            for (int i = 1; i < anzZeilen; i++)
            {
                q[i - 1] = rs[i] / zahlen[i, pivotSpalte];
            }

            // PivotZeile
            double max2 = Double.MaxValue;
            for (int i = 0; i < q.Length; i++)
            {
                if (max2 > q[i])
                {
                    max2 = q[i];
                    pivotZeile = i + 1;
                }
            }
            Console.WriteLine("PivotZeile berechnen: " + max2 + " in Zeile: " + pivotZeile);

            // Werte von PivotSpalte speichern
            for (int i = 0; i < anzZeilen; i++)
            {
                pivotSpalteWerte[i] = zahlen[i, pivotSpalte];
            }

            // PivotElement
            pivotElement = zahlen[pivotZeile, pivotSpalte];
            Console.WriteLine("pivotElement: " + pivotElement);
        }

        public void dividierePivot()
        {
            // Rechte Spalte dividieren gleich dazumachen
            // Zeile dividieren
            for (int i = 0; i < anzXS; i++)
            {
                zahlen[pivotZeile, i] /= pivotElement;
            }
            // pivot --> RS dividieren
            rs[pivotZeile] /= pivotElement;
            // Spalte dividieren
            for (int i = 0; i < anzZeilen; i++)
            {
                if (i != pivotZeile)
                {
                    for (int j = 0; j < anzXS; j++)
                    {
                        //if (i != pivotZeile)
                        //{
                        zahlen[i, j] = zahlen[i, j] - pivotSpalteWerte[i] * zahlen[pivotZeile, j];
                        //}
                    }

                    // RS dividieren
                    rs[i] = rs[i] - pivotSpalteWerte[i] * rs[pivotZeile];
                }
            }
            // Beschriftung anpassen (LINKE SEITE)
            bezLinks[pivotZeile] = "X" + (pivotSpalte+1);
        }

        public bool getState()
        {
            return fertig;
        }

        public override string ToString()
        {
            //  \t --> TAB
            //  \n --> ENTER
            //string ausgabe = "\t";
            StringBuilder sb = new StringBuilder();

            sb.Append("\t");

            foreach(var item in bezOben){
                //ausgabe += item + "\t";
                sb.Append(item + "\t");
            }
            sb.Append("\n");

            for (int i = 0; i < anzZeilen; i++)
            {
                // linke Seite reinschreiben
                sb.Append(bezLinks[i] + "\t");
                // Werte reischreiben
                for (int j = 0; j < anzXS; j++)
                {
                    sb.Append(zahlen[i, j].ToString("0.00") + "\t");
                }
                // rechte Seite
                sb.Append(rs[i].ToString("0.00") + "\t");
                // schauen ob FERTIG --> WENN nicht mehr ausgeben
                if (fertig == false)
                {
                    if (i != 0)
                        sb.Append(q[i - 1].ToString("0.00"));
                }
                // ENTER
                sb.Append("\n");
            }

            return sb.ToString();
        }
    }
}
